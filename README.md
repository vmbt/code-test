The app was scaffolded with the Create React App CLI using npm package manager

The script to run the app is npm run start

The layout of the user dashboard and the modal is based on the provided wireframe. Some functionality ie.

- pagination
- input search logic
  should be added to have a useful prototype, I assumed that wasn't in the scope of the current task, as per requirements provided
