import { useState, useMemo } from "react";
import { Table } from "./components/Table";
import { SearchBar } from "./components/SearchBar";
import { Header } from "./components/Header";
import { Paginator } from "./components/Paginator";
import { Modal } from "./components/Modal";
import "./App.css";

function App() {
  const [isOpen, setIsOpen] = useState(false);
  const [currentId, setCurrentId] = useState(0);

  const toggleModal = (action) => {
    if (action === "open") setIsOpen(true);
    if (action === "close") setIsOpen(false);
  };

  const [data] = require("./sample-data.json");
  const { companyInfo, employees } = data;
  const mapEmployeesToIds = useMemo(
    () =>
      employees.reduce(
        (acc, employee) => ({ ...acc, [employee.id]: employee }),
        {}
      ),
    [employees]
  );

  const employeeData = mapEmployeesToIds[currentId];

  return (
    <div
      className={`App ${isOpen && "blurred"}`}
      onClick={() => toggleModal("close")}
    >
      {isOpen && <div className="layout"></div>}
      {employeeData && (
        <Modal
          isOpen={isOpen}
          toggleModal={toggleModal}
          employeeData={employeeData}
        />
      )}
      <Header companyInfo={companyInfo} />
      <div className="bodyWrapper">
        <SearchBar />
        <Table
          employees={employees}
          isOpen={isOpen}
          toggleModal={toggleModal}
          currentId={currentId}
          setCurrentId={setCurrentId}
        />
        <Paginator />
      </div>
    </div>
  );
}

export default App;
