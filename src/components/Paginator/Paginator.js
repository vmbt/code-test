import { Button } from "../Button";
import styles from "./Paginator.module.css";

export const Paginator = () => {
  return (
    <div className={styles.paginatorWrapper}>
      <div className={styles.paginatorBox}>
        <Button className={styles.button}>Prev</Button>
        <Button className="button indexedButton">1</Button>
        <Button className="button indexedButton">2</Button>
        <Button className="button indexedButton">3</Button>
        <Button className="button indexedButton">4</Button>
        <Button className="button indexedButton">5</Button>
        <Button className={styles.button}>Next</Button>
      </div>
    </div>
  );
};
