import { useMemo } from "react";
import styles from "./Header.module.css";

export const Header = ({
  companyInfo: { companyName, companyMotto, companyEst },
}) => {
  const yearEstablished = useMemo(
    () => new Date(companyEst).getFullYear(),
    [companyEst]
  );

  return (
    <div className={styles.headerWrapper}>
      <div className={styles.headerTitleWrapper}>
        <h1>{companyName}</h1>
        <h4>{companyMotto}</h4>
      </div>
      <div className={styles.headerDescriptionWrapper}>
        <p>Since {yearEstablished}</p>
      </div>
    </div>
  );
};
