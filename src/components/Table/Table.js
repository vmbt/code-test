import styles from "./Table.module.css";

const TableHeader = ({ title, className }) => (
  <th className={className}>
    <div className={styles.tableHeaderInner}>
      <h4>{title}</h4>
      <div className={styles.tableHeaderButtonWrapper}>
        <button
          className={`${styles.tableHeaderButtonUp} ${styles.tableHeaderButton}`}
        />
        <button
          className={`${styles.tableHeaderButtonDown} ${styles.tableHeaderButton}`}
        />
      </div>
    </div>
  </th>
);

const TableCell = ({ children, className }) => {
  return <td className={className}>{children}</td>;
};

export const Table = ({
  employees,
  isOpen,
  toggleModal,
  currentId,
  setCurrentId,
}) => {
  return (
    <div className={styles.tableWrapper}>
      <p className={styles.tableCountParagraph}>{
        // update to page items count when pagination has beed added
        `Showing ${employees.length} of ${employees.length}`
      }</p>
      <table className={styles.table}>
        <thead className={styles.tableRowHeader}>
          <tr>
            <TableHeader
              title="ID"
              className={styles.tableHeader}
            ></TableHeader>
            <TableHeader
              title="Name"
              className={styles.tableHeader}
            ></TableHeader>
            <TableHeader
              title="Contact"
              className={styles.tableHeader}
            ></TableHeader>
            <TableHeader
              title="Address"
              className={styles.tableHeader}
            ></TableHeader>
          </tr>
        </thead>
        <tbody>
          {employees.map((el) => (
            <tr
              className={`${styles.tableRowItems} ${
                el.id === currentId && isOpen && styles.highlighted
              }`}
              key={el.id}
              onClick={(e) => {
                setCurrentId(el.id);
                if (!isOpen) {
                  toggleModal("open");
                  e.stopPropagation();
                  e.preventDefault();
                }
              }}
            >
              <TableCell className={styles.tableCell}>{el.id}</TableCell>
              <TableCell
                className={`${styles.tableCell} ${styles.tableCellName}`}
              >
                <img alt="avatar" src={el.avatar} />
                <>{`${el.firstName} ${el.lastName}`}</>
              </TableCell>
              <TableCell className={styles.tableCell}>{el.contactNo}</TableCell>
              <TableCell className={styles.tableCell}>{el.address}</TableCell>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};
