import "./Button.css";

export const Button = ({ className, children }) => (
  <button className={`button ${className}`}>{children}</button>
);
