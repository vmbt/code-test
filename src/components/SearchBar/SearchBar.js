import styles from "./SearchBar.module.css";
import { Button } from "../Button";

export const SearchBar = () => {
  return (
    <div className={styles.searchBarWrapper}>
      <div className={styles.searchBarBox}>
        <input />
        <Button>Search</Button>
      </div>
    </div>
  );
};
