import styles from "./Modal.module.css";

const onClickHandler = (e, isOpen) => {
  if (isOpen) {
    e.stopPropagation();
    e.preventDefault();
  }
};

export const Modal = ({ isOpen, toggleModal, employeeData }) => {
  return (
    <div
      className={`${styles.modalContainer} ${
        isOpen ? styles.open : styles.close
      }`}
      onClick={(e) => onClickHandler(e, isOpen)}
    >
      {employeeData ? (
        <div className={styles.modalWrapper}>
          <div class={styles.basicDataWrapper}>
            <img alt="avatar" src={employeeData.avatar} />
            <p>{employeeData.jobTitle}</p>
            <p>{employeeData.dateJoined}</p>
          </div>
          <div class={styles.bioDataWrapper}>
            <h1>{`${employeeData.firstName} ${employeeData.lastName}`}</h1>
            <p>{employeeData.bio}</p>
          </div>
        </div>
      ) : null}
      <div
        className={styles.closeButton}
        onClick={() => toggleModal("close")}
      ></div>
    </div>
  );
};
